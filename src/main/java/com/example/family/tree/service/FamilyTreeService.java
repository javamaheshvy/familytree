package com.example.family.tree.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.example.family.tree.Node;
import com.example.family.tree.util.CSVFileReader;

/**
 * 
 * @author Mahesh
 *
 */
public class FamilyTreeService {

	public List<List<Node>> readFile(String file) {
		CSVFileReader csvFileReader = new CSVFileReader();

		return csvFileReader.processInputFile(file);
	}

	public Node createFamilyTree(List<Node> rowNodeList) {

		// Compare by first name and then last name
		Comparator<Node> compareByParentId = Comparator.comparing(Node::getParentId).thenComparing(Node::getId);
		Node rootParentNode = rowNodeList.stream().filter(rowNode -> rowNode.getParentId() == null).findAny().get();

		List<Node> sortedRowNodeList = rowNodeList.stream().filter(rowNode -> rowNode.getParentId() != null)
				.sorted(compareByParentId).collect(Collectors.toList());

		if (rootParentNode != null) {
			sortedRowNodeList.add(0, rootParentNode);
		}

		Node rootNode = null;

		for (int i = 0; i < sortedRowNodeList.size(); i++) {

			Node parentNode = sortedRowNodeList.get(i);

			if (parentNode != null && parentNode.getParentId() == null) {
				rootNode = parentNode;
			}

			for (int j = i + 1; j < sortedRowNodeList.size(); j++) {

				Node node = sortedRowNodeList.get(j);

				if (node != null && node.getParentId() != null && parentNode != null && parentNode.getId() != null
						&& node.getParentId().intValue() == parentNode.getId().intValue()) {
					parentNode.addChild(node);
				}

			}
		}

		return rootNode;
	}

	public void printHierarchicalTree(Node node, boolean blnRootNode, Integer counter, String appender) {
		if (blnRootNode && counter != null) {
			System.out.println("\r\n Family Tree : " + counter);
			System.out.println("\r\n Id : Name" + "\r\n");
		}
		System.out.println(appender + node.getId() + " : " + node.getName());
		node.getChildren().forEach(each -> printHierarchicalTree(each, false, null, appender + appender));
	}

}