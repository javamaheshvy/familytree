package com.example.family.tree.test;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.example.family.tree.Node;
import com.example.family.tree.service.FamilyTreeService;

public class FamilyTreeTest {

	private static final String SPACE = " ";

	private static final String INPUT_TEST_CASES_SUB_FOLDER_PATH = "src/test/resources/inputtestcases";
	private static final String INPUT_FILE_NAME = "data.csv";

	public static void main(String[] args) {
		String projectFolderPath = Paths.get("").toAbsolutePath().toString();

		String file = projectFolderPath + "/" + INPUT_TEST_CASES_SUB_FOLDER_PATH + "/" + INPUT_FILE_NAME;

		FamilyTreeService familyTreeService = new FamilyTreeService();
		
		List<List<Node>> nodeList = familyTreeService.readFile(file);
		
		List<Node> rootNodeList = nodeList.stream().map(rowNodeList -> familyTreeService.createFamilyTree(rowNodeList))
				.collect(Collectors.toList());

		IntStream.range(0, rootNodeList.size()).forEach(index -> familyTreeService.printHierarchicalTree(rootNodeList.get(index), true, (index +  1), SPACE));

	}
}