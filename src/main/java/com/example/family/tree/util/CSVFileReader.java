package com.example.family.tree.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.example.family.tree.Node;

public class CSVFileReader {

	private static final String NULL = "null";
	private static final String COMMA_DELIMITER = ",";
	private static final String PIPE_DELIMITER = "|";

	public List<List<Node>> processInputFile(String inputFilePath) {

		List<List<Node>> inputList = new ArrayList<>();

		try {

			File inputF = new File(inputFilePath);

			InputStream inputFS = new FileInputStream(inputF);

			BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

			// skip the header of the csv
			inputList = br.lines().skip(1).filter(line -> line != null && !line.startsWith("#")).map(mapToNode)
					.collect(Collectors.toList());

			br.close();

		} catch (Exception ex) {
			System.out.println("Exception occurred while reading the csv file " + ex);

		}

		return inputList;

	}

	private Function<String, List<Node>> mapToNode = (line) -> {

		List<Node> rootNodeList = new ArrayList<>();

		try {
			
			// a CSV file has has pipe delimited nodes that represent family members in a family tree. Each node is a CSV with the values being "parent_id, node_id, node_name".

			String[] row = line.split("\\" + PIPE_DELIMITER); 
			if (row != null) {

				for (String member : row) {

					if (member != null) {

						String[] memberFields = member.split(COMMA_DELIMITER);

						try {
							Integer parentId = memberFields[0] != null && !memberFields[0].equalsIgnoreCase(NULL)
									? Integer.parseInt(memberFields[0])
									: null;
							Integer id = memberFields[1] != null ? Integer.parseInt(memberFields[1]) : null;
							String name = memberFields[2];

							Node node = new Node(parentId, id, name);
							rootNodeList.add(node);
						} catch (Exception ex) {
							System.out.println("Exception occurred while processung the data from the csv file " + ex);
						}
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootNodeList;

	};
}