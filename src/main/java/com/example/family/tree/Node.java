package com.example.family.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Mahesh
 *
 */
public class Node {

	private Integer parentId;
	private Integer id;
	private String name;

	private List<Node> children = new ArrayList<>();

	private Node parent;

	public Node(Integer parentId, Integer id, String name) {
		this.parentId = parentId;
		this.id = id;
		this.name = name;
	}

	public Node addChild(Node child) {
		child.setParent(this);
		this.children.add(child);

		return child;
	}
	
	public void addChildren(List<Node> children) {
		children.forEach(each -> each.setParent(this));
		this.children.addAll(children);
	}

	public List<Node> getChildren() {
		return children;
	}

	private void setParent(Node parent) {
		this.parent = parent;
	}

	public Node getParent() {
		return parent;
	}

	public Node getRoot() {

		if (parent == null) {
			return this;
		}

		return parent.getRoot();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "Node [parentId=" + parentId + ", id=" + id + ", name=" + name + "]";
	}
	
	
}