package com.example.family.tree.test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.family.tree.Node;
import com.example.family.tree.service.FamilyTreeService;

public class HierarchicalTreeTest {

	private static final String SPACE = " ";

	private static final String INPUT_TEST_CASES_SUB_FOLDER_PATH = "src/test/resources/inputtestcases";
	private static final String INPUT_FILE_NAME = "data.csv";
	String file = "";

	@BeforeEach
	void setUp() throws Exception {
		String projectFolderPath = Paths.get("").toAbsolutePath().toString();

		file = projectFolderPath + "/" + INPUT_TEST_CASES_SUB_FOLDER_PATH + "/" + INPUT_FILE_NAME;
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreateFamilyTest() {

		FamilyTreeService familyTreeService = new FamilyTreeService();

		List<List<Node>> nodeList = familyTreeService.readFile(file);

		List<Node> rootNodeList = nodeList.stream().map(rowNodeList -> familyTreeService.createFamilyTree(rowNodeList))
				.collect(Collectors.toList());

		for (int i = 0; i < rootNodeList.size(); i++) {

			Node node = rootNodeList.get(i);

			if (i == 0) {
				assertTrue(node.getParent() == null);
				assertEquals(node.getChildren().get(0).getName(), "son");

				assertTrue(node.getChildren().size() == 2);
				assertTrue(node.getChildren().get(1).getChildren().get(0).getChildren().get(0).getId() == 6);

			}
			
			if (i == 1) {
				assertTrue(node.getId().intValue() == 0);
				assertTrue(node.getChildren().get(0).getChildren().get(1).getId() == 4);

				assertTrue(node.getChildren().size() == 2);
				assertTrue(node.getChildren().get(1).getChildren().get(0).getChildren().get(0).getId() == 6);

			}

		}
		
		IntStream.range(0, rootNodeList.size()).forEach(index -> familyTreeService.printHierarchicalTree(rootNodeList.get(index), true, (index +  1), SPACE));

		// rootNodeList.forEach(rootNode -> familyTreeService.printHierarchicalTree(rootNode, true, SPACE));

		// fail("Not yet implemented");
	}

}