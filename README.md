# Instructions for executing the 'Family Tree' application (on Mac/Unix/Linux/Windows system) <br />  


Open a Terminal window on Mac/Linux or MS-DOS (cmd) application (Windows)

md project 
cd project
git clone https://javamaheshvy@bitbucket.org/javamaheshvy/familytree.git 
cd familytree
 
# Maven build and execution instructions

mvn clean 
mvn install 
mvn test

The test results will be displayed

# Executing the application thru .jar file
# Create a jar

mvn package 

# Add FamilyTree-0.0.1.jar to the classpath

java com.example.family.tree.test.FamilyTreeTest

# Execute Junit Test thru IDE 

Open the project FamilyTree in any IDE (Eclipse, Spring Tool Suite, Intellij)
Run the com.example.family.tree.test.HierarchicalTreeTest as Junit Test

com.example.family.tree.test.HierarchicalTreeTest.java is found in src/test/java folder







* Other community or team contact